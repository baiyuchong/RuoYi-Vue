/**
 * 自定义指定注册和获取
 */
import hasRole from './hasRole'
import hasPermi from './hasPermi'

//自定义指令：你仍然需要对普通 DOM 元素进行底层操作，这时候就会用到自定义指令。
// 如果想注册局部指令，组件中也接受一个 directives 的选项
// 然后你可以在模板中任何元素上使用新的自定义property
const install = function(Vue) {
  // 注册或获取全局指令。
  Vue.directive('hasRole', hasRole)
  Vue.directive('hasPermi', hasPermi)
}

if (window.Vue) {
  window['hasRole'] = hasRole
  window['hasPermi'] = hasPermi
  Vue.use(install); // eslint-disable-line
}

export default install
