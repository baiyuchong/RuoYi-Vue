//导入axios进行异步请求
import axios from 'axios'
// 导入element-ui组件
import { Notification, MessageBox, Message } from 'element-ui'
//导入全局状态
import store from '@/store'
// 进行登录认证
import { getToken } from '@/utils/auth'
//导入错误码文件
import errorCode from '@/utils/errorCode'

// 指定将被用在各个请求的配置默认值
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: process.env.VUE_APP_BASE_API,
  // 使用由库提供的配置的默认值来创建实例， 此时超时配置的默认值是 `0`
  // 覆写库的超时默认值,现在，在超时前，所有请求都会等待 10 秒
  timeout: 10000
})

// request请求拦截器
service.interceptors.request.use(config => {
  // 在发送请求之前做些什么
  // 是否设置 token
  const isToken = (config.headers || {}).isToken === false
  if (getToken() && !isToken) {
    //设置了Token
    config.headers['Authorization'] = 'Bearer ' + getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  // get请求映射params参数
  if (config.method === 'get' && config.params) {
    let url = config.url + '?';
    // Object.keys(config.params)获取配置json格式的参数中的所有键值
    for (const propName of Object.keys(config.params)) {
      const value = config.params[propName];
      var part = encodeURIComponent(propName) + "=";
      //获取键所对应的值
      if (value !== null && typeof(value) !== "undefined") {
        if (typeof value === 'object') {
          //键所对应的值为对象，通过Object.keys(value)获取对象的所有键
          for (const key of Object.keys(value)) {
            let params = propName + '[' + key + ']';
            var subPart = encodeURIComponent(params) + "=";
            url += subPart + encodeURIComponent(value[key]) + "&";
          }
        } else {
          url += part + encodeURIComponent(value) + "&";
        }
      }
    }
    // arrayObject.slice(start,end)方法可从已有的数组中返回选定的元素
    // start
    // 必需。规定从何处开始选取。如果是负数，那么它规定从数组尾部开始算起的位置。
    // 也就是说，-1 指最后一个元素，-2 指倒数第二个元素，以此类推。
    // end
    // 可选。规定从何处结束选取。该参数是数组片断结束处的数组下标。
    // 如果没有指定该参数，那么切分的数组包含从 start 到数组结束的所有元素。
    // 如果这个参数是负数，那么它规定的是从数组尾部开始算起的元素。
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }
  return config
}, error => {
   // 对请求错误做些什么
    console.log(error)
    Promise.reject(error)
})

// response响应拦截器
service.interceptors.response.use(res => {
    // 对响应数据做点什么
    // 未设置状态码则默认成功状态
    const code = res.data.code || 200;
    // 获取错误信息
    const msg = errorCode[code] || res.data.msg || errorCode['default']
    if (code === 401) {
      // MessageBox 弹框
      // this.$confirm('此操作将永久删除该文件, 是否继续?', '提示', {
      //   confirmButtonText: '确定',
      //   cancelButtonText: '取消',
      //   type: 'warning'   ----表明消息类型，可以为success，error，info和warning
      // }).then(() =>
      MessageBox.confirm('登录状态已过期，您可以继续留在该页面，或者重新登录', '系统提示', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }
      ).then(() => {
        store.dispatch('LogOut').then(() => {
          location.href = '/index';
        })
      })
    } else if (code === 500) {
      // Message 消息提示
      Message({
        message: msg,
        type: 'error'
      })
      return Promise.reject(new Error(msg))
    } else if (code !== 200) {
      Notification.error({
        title: msg
      })
      return Promise.reject('error')
    } else {
      return res.data
    }
  },
  error => {
    // 对响应错误做点什么
    console.log('err' + error)
    let { message } = error;
    if (message == "Network Error") {
      message = "后端接口连接异常";
    }
    else if (message.includes("timeout")) {
      message = "系统接口请求超时";
    }
    else if (message.includes("Request failed with status code")) {
      message = "系统接口" + message.substr(message.length - 3) + "异常";
    }
    Message({
      message: message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
