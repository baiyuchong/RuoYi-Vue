// js-cookie用来处理cookie相关的插件,import引入
import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

// 获取指定名称的cookie
export function getToken() {
  return Cookies.get(TokenKey)
}

// 创建一个名称为TokenKey，对应值为token的cookie，由于没有设置失效时间，默认失效时间为该网站关闭时
export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

// 删除指定名称的cookie
export function removeToken() {
  return Cookies.remove(TokenKey)
}
