// 用来配置项目中全局状态
import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import tagsView from './modules/tagsView'
import permission from './modules/permission'
import settings from './modules/settings'
import getters from './getters'

Vue.use(Vuex)

// 全局状态（数据）
const store = new Vuex.Store({
  modules: {
    app,
    user,
    tagsView,

    permission,
    // 系统设置全局变量
    settings
  },
  // 用于对Store中的数据进行加工处理形成新的数据，包装数据，不会修改state中的数据；
  // ①Getter可以对Store中已有的数据加工处理之后形成新的数据，类似Vue的计算属性；
  // Store中数据发生变化，Getter的数据也会跟着变化
  //   第一种方式：this.$store.getter.名称
  //   第二种方式：从vuex中导入mapGetter，使用mapGetter将getter中的方法，导入到当前组件的computed方法中；
  getters

})

export default store
