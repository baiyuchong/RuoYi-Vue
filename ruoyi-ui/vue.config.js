'use strict'
// 引入node.js中的path,他的主要作用是将路径转化为绝对路径
const path = require('path')
const defaultSettings = require('./src/settings.js')

function resolve(dir) {
  // __dirname是nodejs的变量，代表当前文件目录绝对路径
  return path.join(__dirname, dir)
}

const name = defaultSettings.title || '若依管理系统' // 标题

const port = process.env.port || process.env.npm_config_port || 80 // 端口

// vue.config.js 配置说明
//官方vue.config.js 参考文档 https://cli.vuejs.org/zh/config/#css-loaderoptions
// 这里只列一部分，具体配置参考文档
// 输出一个大json，这是CMD(commom module definition),主要是给node.js看的
//所有构建工具都是基于nodejs平台运行的，模块化默认采用commonjs，module.exports就是commonjs语法
module.exports = {
  // 部署生产环境和开发环境下的URL。
  // 默认情况下，Vue CLI 会假设你的应用是被部署在一个域名的根路径上
  // 例如 https://www.ruoyi.vip/。如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。例如，如果你的应用被部署在 https://www.ruoyi.vip/admin/，则设置 baseUrl 为 /admin/。
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
  // 在npm run build 或 yarn build 时 ，生成文件的目录名称（要和baseUrl的生产环境路径一致）（默认dist）
  outputDir: 'dist',
  // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下）
  assetsDir: 'static',
  // 是否开启eslint保存检测，有效值：ture | false | 'error'
  lintOnSave: process.env.NODE_ENV === 'development',
  // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
  productionSourceMap: false,

  // webpack-dev-server 相关配置
  // 开发服务器devServer:监视元代码的变动，并自动化（自动编译，自动打开浏览器，自动刷新浏览器~~）
  // 只会在内存中编译打包，不会有任何输出
  //启动devServer指令为：npx webpack-dev-server
  devServer: {
    host: '0.0.0.0',
    //启动端口号
    port: port,
    // 自动打开浏览器
    open: true,
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [process.env.VUE_APP_BASE_API]: {
        target: `http://localhost:8080`,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    },
    disableHostCheck: true
  },
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  chainWebpack(config) {

    //plugins:插件的配置
    config.plugins.delete('preload') // TODO: need test
    config.plugins.delete('prefetch') // TODO: need test

    // set svg-sprite-loader:loader的配置
    // 不同格式文件必须配置不同的loader处理
    config.module
      .rule('svg')
      // 排除一些文件
      .exclude.add(resolve('src/assets/icons'))
      .end()

    // 配置svg的依赖和loader
    config.module
      .rule('icons')
      //test匹配哪些文件
      .test(/\.svg$/)
      // 匹配哪些文件
      .include.add(resolve('src/assets/icons'))
      .end()
      //使用哪些loader进行处理，use数组中的执行顺序：从右向左，先下往上
      .use('svg-sprite-loader')
      //找到.svg资源后，采用那个loder进行处理
      .loader('svg-sprite-loader')
      // .svg文件属性的一些限制
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    config
      .when(process.env.NODE_ENV !== 'development',
        config => {
          // plugins的配置
          config
            .plugin('ScriptExtHtmlWebpackPlugin')
            .after('html')
            //使用哪些插件进行处理，use数组中的执行顺序：从右向左，先下往上
            .use('script-ext-html-webpack-plugin', [{
            // `runtime` must same as runtimeChunk name. default is `runtime`
              inline: /runtime\..*\.js$/
            }])
            .end()

          config
            .optimization.splitChunks({
              chunks: 'all',
              cacheGroups: {
                libs: {
                  name: 'chunk-libs',
                  test: /[\\/]node_modules[\\/]/,
                  priority: 10,
                  chunks: 'initial' // only package third parties that are initially dependent
                },
                elementUI: {
                  name: 'chunk-elementUI', // split elementUI into a single package
                  priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
                  test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
                },
                commons: {
                  name: 'chunk-commons',
                  test: resolve('src/components'), // can customize your rules
                  minChunks: 3, //  minimum common number
                  priority: 5,
                  reuseExistingChunk: true
                }
              }
            })
          config.optimization.runtimeChunk('single'),
          {
            // path.resolve(dir)就是将dir路径转化为绝对路径
             from: path.resolve(__dirname, './public/robots.txt'), //防爬虫文件
             to: './', //到根目录下
          }
        }
      )
  }
}
